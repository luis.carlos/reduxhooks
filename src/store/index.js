import { createStore } from "redux";

/**
 * Estado Inicial
 */
const INITIAL_STATE = {
    data: ["React Native", "ReactJs", "NodeJs"],
};
/**
 * Reducer
 * @param {*} state
 * @param {*} action
 * @returns
 */
function courses(state = INITIAL_STATE, action) {
    switch (action.type) {
        case "ADD_COURSE":
            return { ...state, data: [...state.data, action.title] };
        default:
            return state;
    }
}

/**
 * Store
 */
const store = createStore(courses);

export default store;
